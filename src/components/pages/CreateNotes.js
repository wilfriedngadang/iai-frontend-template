
import React, { Component } from 'react'
import axios from 'axios'

export class CreateNotes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mark: "",
      studentCandidateId: "",
      student_candidateId: "",
      entrance_exam_subjectId: "",
      students: [],
      subjects: [],
    };
  }
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    // axios
    //   .post("http://localhost:8000/entrance_exam_mark", this.state)
    //   .then((response) => {
    //     console.log(response.data);
    //   });
    console.log(this.state);
    // window.location.reload()
  };

   componentDidMount() {
    // const response =  axios.get("http://localhost:8000/student_candidate");
    // this.setState({ students: response.data });
    fetch("http://localhost:8000/student_candidate").then(data => this.setState({
      subjects: data
    }));
  }

  // eslint-disable-next-line no-dupe-class-members
  async componentDidMount() {
    const response = await axios.get("http://localhost:8000/student_candidate");
    this.setState({ students: response.data });
  }

  render() {
    const {
      mark,
      studentCandidateId,
      entrance_exam_subjectId,
      students,
      subjects,
    } = this.state;
    return (
      <main className="main__container">
        <h1> Enter exam marks page </h1>
        <div class="row">
          <div class="col sm-3">
            <form>
              <div class="form-outline mb-4">
                <input
                  type="text"
                  id="mark"
                  value={mark}
                  name="mark"
                  onChange={this.handleChange}
                  class="form-control"
                />
                <label class="form-label" for="form4Example1">
                  Mark
                </label>
              </div>

              <div class="form-outline mb-4">
                <label class="form-label" for="form4Example2">
                  Select Student
                </label>{" "}
                <br />
                <select onChange={this.handleChange} name="studentCandidateId">
                  <option value="">Select the Student</option>
                  {students.map((student) => (
                    <option
                      name="studentCandidateId"
                      value={student.id}
                      key={student.id}
                    >
                      {student.firstName}
                    </option>
                  ))}
                </select>
              </div>

              <div class="form-outline mb-4">
                <label class="form-label" for="form4Example2">
                  Select Student
                </label>{" "}
                <br />
                <select onChange={this.handleChange} name="student_candidateId">
                  <option value="">Select the Student</option>
                  {students.map((student) => (
                    <option
                      name="student_candidateId"
                      value={student.id}
                      key={student.id}
                    >
                      {student.firstName}
                    </option>
                  ))}
                </select>
              </div>

              <div class="form-outline mb-4">
                <label class="form-label" for="form4Example2">
                  Select Subject
                </label>{" "}
                <br />
                <select
                  onChange={this.handleChange}
                  name="entrance_exam_subjectId"
                >
                  <option value="">Select the Subject</option>
                  {subjects.map((subject) => (
                    <option
                      name="student_candidateId"
                      value={subject.id}
                      key={subject.id}
                    >
                      {subject.subject_name}
                    </option>
                  ))}
                </select>
              </div>

              <button
                onClick={this.handleSubmit}
                type="submit"
                class="btn btn-primary btn-block mb-4"
              >
                submit
              </button>
            </form>
          </div>
        </div>
      </main>
    );
  }
}

export default CreateNotes
