import React, { Component } from "react";
import axios from "axios";

export default class CreateSubject extends Component {
  constructor(props) {
    super(props);

    this.state = {
      subject_name: "",
      code: "",
      subject: {}
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const id = this.props.match.params.id
    console.log(id)
     axios
       .put(`http://localhost:8000/entrance_exam_subjects/${id}`, this.state)
       .then((response) => {
         console.log(response.data);
       });
    console.log(this.state);
    window.location.reload()
  };

//   getValue = () => {
//       const id = this.props.match.params.id;
//       axios.get(`http://localhost:3002/entrance_exam_subject/${id}`)
//       .then(response => {
//          this.setState({subject: response.data})
//       });
//        console.log(this.state)
//   }

async componentDidMount() {
    // GET request using axios with async/await
    const id = this.props.match.params.id;
    const response = await axios.get(
      `http://localhost:8000/entrance_exam_subjects/${id}`
    );
    this.setState({ subject: response.data })
}

  render() {
    const { subject_name, code, subject } = this.state;

    return (
      <main className="main__container">
        <h1> Update Subject page </h1>
        <div className="row">
         
          <div className="col sm-3">
            <form>
              <div className="form-outline mb-4">
                <label className="form-label" for="name">
                  Subject Name
                </label>
                <input
                  type="text"
                  id="name"
                  placeholder={subject.subject_name}
                  className="form-control"
                  name="subject_name"
                  value={subject_name}
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-outline mb-4">
                <label className="form-label" for="code">
                  Subject Code
                </label>
                <input
                  type="text"
                  id="code"
                  className="form-control"
                  name="code"
                  placeholder={subject.code}
                  value={code}
                  onChange={this.handleChange}
                />
              </div>

              <button
                type="submit"
                className="btn btn-primary btn-block mb-4"
                onClick={this.handleSubmit}
              >
                Create Subject
              </button>
            </form>
          </div>
        </div>
      </main>
    );
  }
}
