import {useState} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Navbar from "./components/navbar/Navbar"
import Sidebar from "./components/sidebar/Sidebar"
import Main from "./components/main/Main";
import CreateExam from './components/pages/CreateExam';
import ExamList from './components/pages/ExamList';
import ResultList from './components/pages/ResultList';
import CreateResult from './components/pages/CreateResult';
import PLanning from './components/pages/Planning';
import CreatePlanning from './components/pages/CreatePlanning';
import Subject from './components/pages/Subject';
import CreateSubject from './components/pages/CreateSubject';
import EditSubject from './components/pages/EditSubject';
import Notes from './components/pages/Notes';
import CreateNote from './components/pages/CreateNotes'
import EditNote from './components/pages/EditNote'
import EditExam from './components/pages/EditExam'

const App = () => {

  const [sideBarOpen, setSideBarOpen] = useState(false);

  const openSideBar = () => {
    setSideBarOpen(true);
  };

  const closeSideBar = () => {
    setSideBarOpen(false);
  };

  return (
    <Router History={History}>
      <div className="container">
        <Navbar sideBarOpen={sideBarOpen} openSideBar={openSideBar} />
        <Sidebar sideBarOpen={sideBarOpen} closeSideBar={closeSideBar} />

        <Switch>
          <Route exact path="/">
            <Main />
          </Route>
          <Route path="/createExam">
            <CreateExam />
          </Route>
          <Route path="/examList">
            <ExamList />
          </Route>
          <Route path="/resultList">
            <ResultList />
          </Route>
          <Route path="/createResult">
            <CreateResult />
          </Route>
          <Route path="/planning">
            <PLanning />
          </Route>
          <Route path="/createPlanning">
            <CreatePlanning />
          </Route>
          <Route path="/createSubject">
            <CreateSubject />
          </Route>
          <Route path="/subject">
            <Subject />
          </Route>
          <Route
            path="/editSubject/:id"
            render={(props) => <EditSubject {...props} />}
          />
          <Route path="/examNote">
            <Notes />
          </Route>
          <Route path="/createNote">
            <CreateNote />
          </Route>
          <Route
            path="/editNode/:id"
            render={(props) => <EditNote {...props} />}
          />
          <Route
            path="/editExam/:id"
            render={(props) => <EditExam {...props} />}
          />
          <Route path="/examNote">
            <CreateNote />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
