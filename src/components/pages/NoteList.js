
import axios from 'axios';
import {Link} from 'react-router-dom';
import React, { Component } from 'react'

export class NoteList extends Component {
  render() {
    return (
      <main className="main__container">
        <h1> Student Marks List page </h1>
        <Link class="btn btn-primary" to={"/createNote"}>
          {" "}
          Add Student Mark
        </Link>
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Marks</th>
              <th scope="col">Subject</th>
              <th scope="col">Candidate</th>
              <th scope="col">Exam</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
          
              <tr>
                <th scope="row"></th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                
                </td>
              </tr>
            
          </tbody>
        </table>
      </main>
    );
  }
}

export default NoteList
