import React,{useEffect, useState} from 'react'
import './pages.css';
import './style.css'
import axios from 'axios'
import {Link} from 'react-router-dom';

function ExamList() {

  const [exams, setSubject] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const req = await axios.get("http://localhost:8000/entrance_exam");

      setSubject(req.data);
    }
    fetchData();
  }, []);

  const deleteData = (id) => {
    axios
      .delete(`http://localhost:8000/entrance_exam/${id}`)
      .then((res) => {
        console.log(res);
      });
    window.location.reload();
  }; 

    return (
      <main className="main__container">
        <h1> Exam List</h1>
        {/* link to the page which creates a new subject */}
        <Link className="btn btn-primary" to={"/createExam"}>
          Create An Exam
        </Link>
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Exam Session</th>
              <th scope="col">Academic year </th>
              <th scope="col">Exam Date</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {exams.map((exam) => (
              <tr key={exam.id}>
                <th scope="row">{exam.id}</th>
                <td>{exam.sesion}</td>
                <td>{exam.academicYear}</td>
                <td>{exam.examDate}</td>
                <td>
                  <Link
                    className="btn btn-secondary"
                    to={{
                      pathname: `/editExam/${exam.id}`,
                      id: exam.id,
                    }}
                  >
                    Edit
                  </Link>

                  <button
                    onClick={(e) => deleteData(exam.id)}
                    class="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </main>
    );
}

export default ExamList
