import React, { Component } from 'react'
import axios from 'axios'

export default class CreateSubject extends Component {

   
  constructor(props) {
    super(props)
  
    this.state = {
      subject_name: "",
      code: "",
    };
  }

  handleChange = (e) => {
      this.setState({
        [e.target.name]: e.target.value
      })
    } 

    handleSubmit = (e) => {
      e.preventDefault();
       axios
         .post("http://localhost:8000/entrance_exam_subjects", this.state)
         .then((response) => {
           console.log(response.data);
         });
       console.log(this.state);
       window.location.reload()
    }
  

  render() {

    const { subject_name, code } = this.state;

    return (
      <main className="main__container">
        <h1> Create Subject page </h1>
        <div className="row">
          <div className="col sm-3">
            <form >
              <div className="form-outline mb-4">
                <label className="form-label" for="name">
                  Subject Name
                </label>
                <input
                  type="text"
                  id="name"
                  className="form-control"
                  name="subject_name"
                  value={subject_name}
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-outline mb-4">
                <label className="form-label" for="code">
                  Subject Code
                </label>
                <input
                  type="text"
                  id="code"
                  className="form-control"
                  name="code"
                  value={code}
                  onChange={this.handleChange}
                />
              </div>

              <button
                type="submit"
                className="btn btn-primary btn-block mb-4"
                onClick={this.handleSubmit}
              >
                Create Subject
              </button>
            </form>
          </div>
        </div>
      </main>
    );
  }
}
