import "./Sidebar.css";
import logo from "../../assets/lofo.png"
import { Link } from 'react-router-dom';


const Sidebar = ({ sideBarOpen, closeSideBar}) => {
    return (
      <div className={sideBarOpen ? "sidebar-responsive" : ""} id="sidebar">
        <div className="sidebar__title">
          <div className="sidebar__img">
            <img src={logo} alt="logo" />
            <h1>IAI Concour</h1>
          </div>
          <i
            className="fa fa-times"
            id="sidebarIcon"
            onClick={() => closeSideBar()}
          ></i>
        </div>

        <div className="sidebar__menu">
          <div className="sidebar__link active_menu_link">
            <i className="fa fa-home"></i>
            <Link to={"/"}>Dashbaord</Link>
          </div>
          <h2>Entrance Exam Module</h2>
         
          <div className="sidebar__link">
            <i className="fa fa-building-o"></i>
            <Link to={"/examList"}>Manage Exams</Link>
          </div>
          <div className="sidebar__link">
            <i className="fa fa-building-o"></i>
            <Link to={"/examNote"}>Manage Notes</Link>
          </div>
          <div className="sidebar__link">
            <i className="fa fa-building-o"></i>
            <Link to={"/subject"}>Manage Subjects</Link>
          </div>
          <h2>RESULTS</h2>
          {/* <div className="sidebar__link">
            <i className="fa fa-question"></i>
            <Link to={"/createResult"}>Add Result</Link>
          </div> */}
          <div className="sidebar__link">
            <i className="fa fa-sign-out"></i>
            <Link to={"/resultList"}>Result List</Link>
          </div>

          <h2>Planning</h2>
          <div className="sidebar__link">
            <i className="fa fa-money"></i>
            <Link to={"/createPlanning"}> Add Planning</Link>
          </div>
          <div className="sidebar__link">
            <i className="fa fa-briefcase"></i>
            <Link to={"/planning"}>Planning</Link>
          </div>
          <div className="sidebar__logout">
            <i className="fa fa-power-off"></i>
            <Link to={"/logout"}>Log out</Link>
          </div>
        </div>
      </div>
    );
}

export default Sidebar;