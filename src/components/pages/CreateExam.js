import React, { Component } from "react";
import axios from "axios";

export default class CreateSubject extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sesion: "",
      accademic_yearId: '',
      examDate: "",
      academicYear: "",
      years: []
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/entrance_exam", this.state)
      .then((response) => {
        console.log(response.data);
      });
    console.log(this.state);
    //window.location.reload()
  };


  async componentDidMount() {
    const response = await axios.get("http://localhost:8000/accademic_year");
    this.setState({ years: response.data})
  }


  render() {
    const { sesion, examDate , academicYear , years} = this.state;

    return (
      <main className="main__container">
        <h1> Create An Exam</h1>
        <div class="row">
          <div class="col sm-3">
            <form>
              <div class="form-outline mb-4">
                <label class="form-label" for="form4Example1">
                  Exam Session
                </label>
                <input
                  type="text"
                  name="sesion"
                  value={sesion}
                  onChange={this.handleChange}
                  id="form4Example1"
                  class="form-control"
                />
              </div>

        
              <div class="form-outline mb-4">
                <label class="form-label" for="form4Example2">
                  Exam Date
                </label>
                <input
                  type="text"
                  name="examDate"
                  value={examDate}
                  onChange={this.handleChange}
                  id="form4Example2"
                  class="form-control"
                />
              </div>

              <div class="form-outline mb-4">
                <label class="form-label" for="form4Example2">
                  Academic Year
                </label>
                <input
                  type="text"
                  name="academicYear"
                  value={academicYear}
                  onChange={this.handleChange}
                  id="form4Example2"
                  class="form-control"
                />
              </div>
              <div class="form-outline mb-4">
                <label class="form-label" for="form4Example2">
                  Exam Year
                </label>{" "}
                <br />
                <select onChange={this.handleChange} name="accademic_yearId">
                  <option value=""> Select the Accademic Year </option>
                  {years.map((year) => (
                    <option
                      name="accademic_yearId"
                      key={year.id}
                      value={year.id}
                    >
                      {year.beginDate}
                    </option>
                  ))}
                </select>
              </div>

              <button
                type="submit"
                onClick={this.handleSubmit}
                class="btn btn-primary btn-block mb-4"
              >
                Create Exam
              </button>
            </form>
          </div>
        </div>
      </main>
    );
  }
}
