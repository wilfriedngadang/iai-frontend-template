import React, {useState, useEffect} from 'react'
import axios from 'axios';
//import { generatePath, useHistory } from 'react-router';
import { Link } from 'react-router-dom';

function Subject() {

  const [subjects, setSubject] = useState([]); 


  useEffect(() => {
    async function fetchData() {
      const req = await axios.get(
        "http://localhost:8000/entrance_exam_subjects"
      );

      setSubject(req.data);
    }
    fetchData();
  }, [])

   const deleteData = (id, e) => {
     axios
       .delete(`http://localhost:8000/entrance_exam_subjects/${id}`)
       .then((res) => {
         console.log(res);
       });
       window.location.reload()
   };  

  //  const history = useHistory();
  //  const handleProceed = (id) => {
  //    id && history.push(generatePath("/editSubject/:id", {id}));
  //  }

    return (
      <main className="main__container">
        <h1> Subject List page </h1>
       {/* link to the page which creates a new subject */}
        <Link className="btn btn-primary" to={"/createSubject"}> Create Subject</Link>
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Subject Name</th>
              <th scope="col">Subject Code</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {subjects.map((subject) => (
              <tr>
                <th scope="row">{subject.id}</th>
                <td>{subject.subject_name}</td>
                <td>{subject.code}</td>
                <td>
                  {/* <button onClick = {handleProceed(subject.id)} class="btn btn-secondary">Edit</button> */}
                  <Link
                    className="btn btn-secondary"
                    to={{
                      pathname: `/editSubject/${subject.id}`,
                      id: subject.id,
                    }}
                  >
                    Edit
                  </Link>
                  <button
                    onClick={(e) => deleteData(subject.id, e)}
                    class="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </main>
    );
}

export default Subject
