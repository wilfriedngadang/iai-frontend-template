import React,{useEffect, useState}from 'react'
import axios from 'axios';
import { Link } from 'react-router-dom'

function Planning() {

   const [plannings, setPlanning] = useState([]);

   useEffect(() => {
     async function fetchData() {
       const req = await axios.get("http://localhost:3002/examPlanning");

       setPlanning(req.data);
     }
     fetchData();
   }, []);

    return (
      <main className="main__container">
        <h1>Planning </h1>
        <Link to={"/createPlanning"} class="btn btn-primary">
         
          Creat Planning
        </Link>
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Date</th>
              <th scope="col">Branch</th>
              <th scope="col">Subject</th>
              <th scope="col">Exam</th>
            </tr>
          </thead>
          <tbody>
            {plannings.map((planning) => (
              <tr>
                <th scope="row">{planning.id}</th>
                <td>{planning.planing_date}</td>
                <td>{planning._branch}</td>
                <td>{planning._entrance_exam_subject}</td>
                <td>{planning._entrance_exam}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </main>
    );
}

export default Planning
