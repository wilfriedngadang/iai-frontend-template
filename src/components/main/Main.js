import {useEffect, useState} from 'react'
import "./Main.css"
import favicon from "../../assets/favicon.svg";
import axios from 'axios';
//import Chart from "../charts/Chart";

const Main = () => {

    const [studentcount, setStudentCount] = useState([]);
    const [sesioncount, setSesiionCount] = useState([]);
    const [totalCandidate, setTotalCandidate] = useState([]);


    useEffect(() => {
      async function fetchData() {
        const req = await axios.get(
          "http://localhost:3002/studentcount"
        );

        setStudentCount(req.data);
      }
      fetchData();
    }, []);

    useEffect(() => {
      async function fetchData() {
        const req = await axios.get("http://localhost:3002/entranceExamCount");

        setTotalCandidate(req.data);
      }
      fetchData();
    }, []);
    
    // consuming the endpoind of the api which gets the total number of candidates
     useEffect(() => {
       async function fetchData() {
         const req = await axios.get("http://localhost:8000/numbertPassed");

         setStudentCount(req.data);
       }
       fetchData();
     }, []);
    return (
      <main>
        <div className="main__container">
          <div className="main__title">
            <img src={favicon} alt="favicon" />
            <div className="main__greetinng">
              <h1>Hello Admin</h1>
              <p>Welcome to the concour management module dashboard</p>
            </div>
          </div>

          <div className="main__cards">
            <div className="card">
              <i className="fa fa-user-o fa-2x text-lightblue"></i>
              <div className="card_inner">
                <p className="text-primary-p">Number of Candidates</p>
                <span className="font-bold text-title">{studentcount}</span>
              </div>
            </div>

            <div className="card">
              <i className="fa fa-calendar fa-2x text-red"></i>
              <div className="card_inner">
                <p className="text-primary-p">Number of Sessions</p>
                <span className="font-bold text-title">{sesioncount}</span>
              </div>
            </div>

            <div className="card">
              <i className="fa fa-video-camera fa-2x text-yellow"></i>
              <div className="card_inner">
                <p className="text-primary-p">Number of Students</p>
                <span className="font-bold text-title">{studentcount}</span>
              </div>
            </div>

            <div className="card">
              <i className="fa fa-thumbs-up fa-2x text-green"></i>
              <div className="card_inner">
                <p className="text-primary-p">Number of Success</p>
                <span className="font-bold text-title">{totalCandidate}</span>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
}

export default Main